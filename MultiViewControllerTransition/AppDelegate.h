//
//  AppDelegate.h
//  MultiViewControllerTransition
//
//  Created by ShogoMizumoto on 2014/08/26.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end