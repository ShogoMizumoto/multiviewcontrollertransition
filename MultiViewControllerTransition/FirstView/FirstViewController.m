//
//  FirstViewController.m
//  MultiViewControllerTransition
//
//  Created by ShogoMizumoto on 2014/08/26.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "FirstViewController.h"
#import "SecondViewController.h"
#import "ThirdViewController.h"

@implementation FirstViewController

- (void)viewDidLoad
{
    NSLog(@"FirstView : viewDidLoad.");

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"FirstView : viewWillAppear.");

    [super viewWillAppear:animated];
}

- (IBAction)tapBtn
{
    SecondViewController *secondViewController = [SecondViewController new];
    ThirdViewController  *thirdViewController  = [ThirdViewController new];

    [self.navigationController pushViewController:secondViewController animated:NO];
    [self.navigationController pushViewController:thirdViewController animated:YES];
}

@end
