//
//  SecondViewController.m
//  MultiViewControllerTransition
//
//  Created by ShogoMizumoto on 2014/08/26.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "SecondViewController.h"

@implementation SecondViewController

- (void)viewDidLoad
{
    NSLog(@"SecondView : viewDidLoad.");

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"SecondView : viewWillAppear.");

    [super viewWillAppear:animated];
}

@end
