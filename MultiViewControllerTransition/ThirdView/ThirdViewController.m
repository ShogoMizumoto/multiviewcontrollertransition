//
//  ThirdViewController.m
//  MultiViewControllerTransition
//
//  Created by ShogoMizumoto on 2014/08/26.
//  Copyright (c) 2014 ShogoMizumoto. All rights reserved.
//

#import "ThirdViewController.h"

@implementation ThirdViewController

- (void)viewDidLoad
{
    NSLog(@"ThirdView : viewDidLoad.");

    [super viewDidLoad];
}

- (void)viewWillAppear:(BOOL)animated
{
    NSLog(@"ThirdView : viewWillAppear.");

    [super viewWillAppear:animated];
}

@end
